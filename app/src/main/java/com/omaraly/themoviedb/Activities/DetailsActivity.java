package com.omaraly.themoviedb.Activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.omaraly.themoviedb.Models.Movies;
import com.omaraly.themoviedb.R;
import com.omaraly.themoviedb.Utilities.Utilities;
import com.omaraly.themoviedb.databinding.ActivityDetailsBinding;

public class DetailsActivity extends AppCompatActivity {

    ActivityDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null) {
            Movies.ResultsBean model = (Movies.ResultsBean) bundle.getSerializable("model");
            setUPData(model);

        }

        onClick();
    }

    void onClick() {


        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Add to Favorite (Test Only)", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    void setUPData(Movies.ResultsBean model) {
        Utilities.downLoadImage(binding.image, model.poster_path);
        binding.toolbar.setTitle(model.title);
        binding.toolbarLayout.setTitle(model.title);
        binding.Name.setText(model.title);
        binding.Overview.setText(model.overview);
        binding.votecount.setText(model.vote_count + "");
        binding.voteAverage.setText(model.vote_average + "");
        binding.Popularity.setText(model.popularity + "");
        binding.ReleaseDate.setText(model.release_date + "");
    }
}
