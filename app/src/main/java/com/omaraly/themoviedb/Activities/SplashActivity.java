package com.omaraly.themoviedb.Activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.omaraly.themoviedb.R;
import com.omaraly.themoviedb.SharedPreferences.ConfigurationFile;
import com.omaraly.themoviedb.Utilities.IntentClass;
import com.omaraly.themoviedb.databinding.ActivitySplashBinding;


public class SplashActivity extends AppCompatActivity {

    private ActivitySplashBinding activitySplashBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        activity = this;


        ConfigurationFile.setCurrentLanguage(activity, "en");

        activitySplashBinding.title.setVisibility(View.GONE);
        activitySplashBinding.image.setVisibility(View.GONE);


        final Animation myanim = AnimationUtils.loadAnimation(activity, R.anim.lefttoright);
        final Animation animation = AnimationUtils.loadAnimation(activity, R.anim.from_top);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                activitySplashBinding.title.setVisibility(View.VISIBLE);
                activitySplashBinding.image.setVisibility(View.VISIBLE);
                activitySplashBinding.title.startAnimation(myanim);
                activitySplashBinding.image.setAnimation(animation);

            }
        }, 500);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                IntentClass.goToActivityAndClear(activity, MainActivity.class, null);

            }
        }, 2500);


    }


}