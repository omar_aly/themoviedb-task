package com.omaraly.themoviedb.Activities;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.omaraly.themoviedb.API.APIModel;
import com.omaraly.themoviedb.Adapters.MoviesAdapter;
import com.omaraly.themoviedb.Adapters.MoviesAdapterFullWidth;
import com.omaraly.themoviedb.Models.Movies;
import com.omaraly.themoviedb.R;
import com.omaraly.themoviedb.Utilities.Dialogs;
import com.omaraly.themoviedb.Utilities.LoadingDialog;
import com.omaraly.themoviedb.databinding.ActivityMainBinding;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    MainActivity activity;
    int page = 1;
    public ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activity = this;
        Dialogs.showSnackbarText(binding.lisTopRated, "Scroll right and left in “Popular” and down in “Top rated”");
        binding.popular.setVisibility(View.GONE);
        binding.topRated.setVisibility(View.GONE);
        getPopular();
    }

    public void getPopular() {

        APIModel.getMethod(activity, "popular?api_key=" + APIModel.KEY + "&language=en-US&page=" + page, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                binding.framProgressBar.setVisibility(View.VISIBLE);
                super.onStart();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                binding.framProgressBar.setVisibility(View.GONE);

                APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        getPopular();
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("popular", responseString + "");
                Movies data = null;
                try {
                    Type dataType = new TypeToken<Movies>() {
                    }.getType();
                    data = new Gson().fromJson(responseString, dataType);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
                binding.popular.setVisibility(View.VISIBLE);

                setupRecyclerView(data.results, 1);
                binding.framProgressBar.setVisibility(View.GONE);
                getTopRated();

            }

            @Override
            public void onFinish() {

                super.onFinish();

            }
        });
    }

    public void getTopRated() {


        APIModel.getMethod(activity, "top_rated?api_key=" + APIModel.KEY + "&language=en-US&page=" + page, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                binding.framProgressBar.setVisibility(View.VISIBLE);
                super.onStart();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(activity, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        getTopRated();
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("top_rated", responseString + "");

                Movies data = null;
                try {
                    Type dataType = new TypeToken<Movies>() {
                    }.getType();
                    data = new Gson().fromJson(responseString, dataType);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
                binding.topRated.setVisibility(View.VISIBLE);

                setupRecyclerView(data.results, 2);
            }

            @Override
            public void onFinish() {
                binding.framProgressBar.setVisibility(View.GONE);

                super.onFinish();

            }
        });
    }

    public void setupRecyclerView(List<Movies.ResultsBean> list, int type) {

        if (type == 1) {
            if (list.size() > 0) {
                MoviesAdapter mAdapter = new MoviesAdapter(activity, list);
                binding.listPopular.setAdapter(mAdapter);

            }
        } else if (type == 2) {
            if (list.size() > 0) {

                MoviesAdapterFullWidth mAdapter = new MoviesAdapterFullWidth(activity, list);
                binding.lisTopRated.setAdapter(mAdapter);

            }
        }
    }


}
