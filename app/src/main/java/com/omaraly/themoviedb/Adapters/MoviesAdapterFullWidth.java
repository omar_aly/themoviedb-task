package com.omaraly.themoviedb.Adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.omaraly.themoviedb.Activities.DetailsActivity;
import com.omaraly.themoviedb.Models.Movies;
import com.omaraly.themoviedb.R;
 import com.omaraly.themoviedb.Utilities.IntentClass;
import com.omaraly.themoviedb.Utilities.Utilities;

import java.util.ArrayList;
import java.util.List;


public class MoviesAdapterFullWidth extends RecyclerView.Adapter<MoviesAdapterFullWidth.MyViewHolder> {


    Activity activity;
    public List<Movies.ResultsBean> list = new ArrayList<>();


    public MoviesAdapterFullWidth(Activity activity, List<Movies.ResultsBean> list) {
        this.activity = activity;
        this.list = list;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movies_full_width, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Movies.ResultsBean dataBean = list.get(position);


        holder.name.setText(dataBean.original_title);
        holder.voteAverage.setText(dataBean.vote_average+"");
        holder.overview.setText(dataBean.overview);
        Utilities.downLoadImage(holder.image,   dataBean.poster_path);


        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("model" , dataBean);
                IntentClass.goToActivity(activity , DetailsActivity.class , bundle);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder {


        private ImageView image;
        private TextView name;
        private TextView overview;
        private TextView voteAverage;
        private MaterialCardView card;


        public MyViewHolder(View view) {
            super(view);

            image = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            overview = (TextView) itemView.findViewById(R.id.overview);
            voteAverage = (TextView) itemView.findViewById(R.id.vote_average);
            card = (MaterialCardView) itemView.findViewById(R.id.card);

        }


    }


}
