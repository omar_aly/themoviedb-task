package com.omaraly.themoviedb.Utilities;



import android.widget.ImageView;

import com.omaraly.themoviedb.API.APIModel;
import com.squareup.picasso.Picasso;



public class Utilities {


    public static void downLoadImage(ImageView imageView, String url) {
        if (!url.equals("")) {
            url = APIModel.IMAGE_LINK + url ;
            Picasso.get()
                    .load(url)
                    .into(imageView);
        }
    }


}
