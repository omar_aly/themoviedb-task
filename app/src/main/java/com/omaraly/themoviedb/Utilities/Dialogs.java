package com.omaraly.themoviedb.Utilities;


import android.content.Context;
 import android.graphics.Color;

import android.support.design.widget.Snackbar;
 import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.omaraly.themoviedb.API.APIModel;



public abstract class Dialogs {






    public static void showSnackbarText(View view, String message) {


        if (view != null) {
            Snackbar snackbar = Snackbar
                    .make(view, message, Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            int snackbarTextId = android.support.design.R.id.snackbar_text;
            TextView textView = (TextView) snackbarView.findViewById(snackbarTextId);
            textView.setTextColor(Color.parseColor("#ffffff"));
            snackbarView.setBackgroundColor(Color.BLACK);
            snackbar.show();
        }
    }

    public static void showSnackbarRefresh(View view, String message, final APIModel.RefreshTokenListener listener) {


        if (view != null) {
            Snackbar snackbar = Snackbar
                    .make(view, message, Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            int snackbarTextId = android.support.design.R.id.snackbar_text;
            TextView textView = (TextView) snackbarView.findViewById(snackbarTextId);
            textView.setTextColor(Color.parseColor("#ffffff"));
            snackbarView.setBackgroundColor(Color.BLACK);
            snackbar.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onRefresh();
                }
            });
            snackbar.setActionTextColor(Color.parseColor("#00d276"));
            snackbar.show();
        }
    }


}
