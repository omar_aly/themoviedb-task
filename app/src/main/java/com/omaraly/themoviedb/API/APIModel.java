package com.omaraly.themoviedb.API;


import android.app.Activity;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.TextHttpResponseHandler;

import com.omaraly.themoviedb.Activities.MainActivity;
import com.omaraly.themoviedb.R;
import com.omaraly.themoviedb.Utilities.Dialogs;


import java.security.KeyStore;


public class APIModel {

    public final static String BASE_URL = "https://api.themoviedb.org/3/movie/";
    public final static String KEY = "35a596610f4d406733e2c424e59ef7f1";
    public static String IMAGE_LINK = "https://image.tmdb.org/t/p/w500/";

    public static void handleFailure(final MainActivity activity, int statusCode, String errorResponse, final RefreshTokenListener listener) {
        Log.e("fail", statusCode + "--" + errorResponse);


        switch (statusCode) {


            default:
                Dialogs.showSnackbarRefresh(activity.binding.lisTopRated, activity.getString(R.string.no_network), listener);
                break;


        }
    }


    public static AsyncHttpClient getMethod(Activity currentActivity, String url, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        KeyStore trustStore = null;
        try {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            MySSLSocketFactory socketFactory = new MySSLSocketFactory(trustStore);
            socketFactory.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            client.setSSLSocketFactory(socketFactory);
        } catch (Exception e) {
            e.printStackTrace();
        }

        client.addHeader("Accept", "application/json");
        client.addHeader("User-Agent", "PostmanRuntime/7.2.0");
        Log.e("BASE_URL", BASE_URL + url);
        client.get(BASE_URL + url, textHttpResponseHandler);

        return client;
    }


    public interface RefreshTokenListener {
        void onRefresh();
    }


}